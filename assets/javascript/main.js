(function() {
  'use strict'

  var app = {} || window;

  app.init = function() {
    this.menu();
  }
  app.status = 0;

  app.menu = function() {
    var menu = document.getElementsByClassName('nav-content'),
        button = document.getElementsByClassName('nav-button');

    button[0].addEventListener('click', function() {app.actions(menu)}, false);
    console.log(button);
  }

  app.actions = function(menu) {
    if (app.status == 0) {
      menu[0].style.display = 'block';
      app.status = 1;
      return;
    } 
    menu[0].style.display = 'none';
    app.status = 0;
    console.log('Fechado');
    return;
  }

  app.init();
})();